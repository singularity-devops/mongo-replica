# mongo-app

## Execution

- Run and build containers using **docker-compose**
- Enter Mongo Shell in the primary container
- Run the commands in **config.js**
- Restore the data from dump using **mongorestore**

## Screenshots

### mongorestore

![mongorestore](https://user-images.githubusercontent.com/54265853/182124352-3b891be0-b48d-4732-a055-9893dbda2bd8.png)

### mongo-2

![mongo1](https://user-images.githubusercontent.com/54265853/182371523-5d594b5e-6124-466d-bfc8-f3f2baeaf925.png)
